//
//  CashWithdrawalViewController.swift
//  NanoBank
//
//  Created by Oktavia Citra on 22/09/21.
//

import Cocoa

class CashWithdrawalViewController: NSViewController {
    var account: Account?
    @IBOutlet var currencyDropdown: NSPopUpButton!
    @IBOutlet var amountTextfield: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currencyDropdown.removeAllItems()
        currencyDropdown.addItems(withTitles: ["Dollar", "Rupiah"])
        // Do view setup here.
    }
    
    @IBAction func save(_ sender: Any) {
        if let amount: Float = Float(amountTextfield.stringValue) {
            let index: Int = currencyDropdown.indexOfSelectedItem
            if let account: Account = account {
                let processed: Bool = index == 0 ? account.takeInDollar(amount: amount) : account.takeInRupiah(amount: amount)
                if !processed {
                    showAlert()
                }
                back()
                
            }
            
        }
    }
    
    func showAlert() {
        let alert = NSAlert()
        alert.messageText = "Transaksi Gagal"
        alert.informativeText = "Saldo anda kurang dari jumlah yang anda akan ambil"
        alert.addButton(withTitle: "OK")
        alert.alertStyle = .warning
        alert.runModal()
    }
    
    private func back() {
        if let controller = self.storyboard?.instantiateController(withIdentifier: "MenuView") as? MenuViewController {
            controller.account = account
            self.view.window?.contentViewController = controller
        }
    }
}
