//
//  MenuViewController.swift
//  NanoBank
//
//  Created by Oktavia Citra on 22/09/21.
//

import Cocoa

class MenuViewController: NSViewController {
    @IBOutlet var greetingLabel: NSTextField!
    var account: Account?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let name: String = account?.getName() {
            greetingLabel.stringValue = "Selamat datang, " + name + "!"
        }
    }
    
    @IBAction func checkSaldo(_ sender: Any) {
        if let controller = self.storyboard?.instantiateController(withIdentifier: "CheckSaldoView") as? CheckSaldoViewController {
            controller.account = account
            self.view.window?.contentViewController = controller
        }
    }
    
    @IBAction func cashDeposit(_ sender: Any) {
        if let controller = self.storyboard?.instantiateController(withIdentifier: "CashDepositView") as? CashDepositViewController {
            controller.account = account
            self.view.window?.contentViewController = controller
        }
    }
    
    @IBAction func cashWithdrawal(_ sender: Any) {
        if let controller = self.storyboard?.instantiateController(withIdentifier: "CashWithdrawalView") as? CashWithdrawalViewController {
            controller.account = account
            self.view.window?.contentViewController = controller
        }
    }
    
    @IBAction func exit(_ sender: Any) {
        if let controller = self.storyboard?.instantiateController(withIdentifier: "LoginView") as? LoginViewController {
            self.view.window?.contentViewController = controller
        }
    }
    
    
}
