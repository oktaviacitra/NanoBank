//
//  Account.swift
//  NanoBank
//
//  Created by Oktavia Citra on 22/09/21.
//

import Foundation

class Account {
    private var name: String?
    private var pin: String?
    private var saldo: Float?
    
    init(name: String, pin: String, saldo: Float) {
        self.name = name
        self.pin = pin
        self.saldo = saldo
    }
    
    func getName() -> String {
        if let nameTemp: String = name {
            return nameTemp
        }
        return ""
    }
    
    func getPin() -> String {
        if let pinTemp: String = pin {
            return pinTemp
        }
        return ""
    }
    
    func getSaldo() -> Float {
        if let saldoTemp: Float = saldo {
            return saldoTemp
        }
        return 0.0
    }
    
    func save(amount: Float) {
        saldo? += amount
    }
    
    func takeInRupiah(amount: Float) -> Bool {
        var result: Bool = false
        if let saldoTemp: Float = saldo {
            if saldoTemp >= amount {
                saldo? -= amount
                result = true
            }
        }
        return result
    }
    
    func takeInDollar(amount: Float) -> Bool {
        let amountInRupiah: Float = (amount * 15000) + (amount * 50)
        return takeInRupiah(amount: amountInRupiah)
    }
}
