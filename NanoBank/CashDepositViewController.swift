//
//  CashDepositViewController.swift
//  NanoBank
//
//  Created by Oktavia Citra on 22/09/21.
//

import Cocoa

class CashDepositViewController: NSViewController {
    var account: Account?
    @IBOutlet var amountTextfield: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
    @IBAction func save(_ sender: Any) {
        if let amount: Float = Float(amountTextfield.stringValue) {
            account?.save(amount: amount)
        }
        back()
    }
    
    private func back() {
        if let controller = self.storyboard?.instantiateController(withIdentifier: "MenuView") as? MenuViewController {
            controller.account = account
            self.view.window?.contentViewController = controller
        }
    }
    
}
