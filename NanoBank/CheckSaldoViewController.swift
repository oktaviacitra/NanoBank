//
//  CheckSaldoViewController.swift
//  NanoBank
//
//  Created by Oktavia Citra on 22/09/21.
//

import Cocoa

class CheckSaldoViewController: NSViewController {
    @IBOutlet var saldoLabel: NSTextField!
    var account: Account?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let saldo: Float = account?.getSaldo() {
            saldoLabel.stringValue = "Rp" + format(number: saldo) + ",-"
        }
    }
    
    func format(number: Float) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        if let result = formatter.string(from: NSNumber(value: number)) {
            return result
        }
        return ""
    }
    
    @IBAction func done(_ sender: Any) {
        if let controller = self.storyboard?.instantiateController(withIdentifier: "MenuView") as? MenuViewController {
            controller.account = account
            self.view.window?.contentViewController = controller
        }
    }
    
}
