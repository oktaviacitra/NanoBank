//
//  LoginViewController.swift
//  NanoBank
//
//  Created by Oktavia Citra on 22/09/21.
//

import Cocoa

class LoginViewController: NSViewController {
    @IBOutlet var nameTextfield: NSTextField!
    @IBOutlet var pinTextfield: NSSecureTextField!
    var accounts: [Account] = [Account]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeData()
    }
    
    func initializeData() {
        accounts.append(Account(name: "Citra", pin: "1910", saldo: 1039020.0))
        accounts.append(Account(name: "Badai", pin: "1234", saldo: 25440000.0))
    }
    
    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    @IBAction func login(_ sender: Any) {
        let name: String = nameTextfield.stringValue
        let pin: String = pinTextfield.stringValue
        
        let account: Account = validation(name: name, pin: pin)
        if account.getName() == "" {
            let alert = NSAlert()
            alert.messageText = "Gagal Masuk"
            alert.informativeText = "Cek kembali nama dan pin anda, apakah sudah sesuai"
            alert.addButton(withTitle: "OK")
            alert.alertStyle = .warning
            alert.runModal()
        } else {
            if let controller = self.storyboard?.instantiateController(withIdentifier: "MenuView") as? MenuViewController {
                controller.account = account
                self.view.window?.contentViewController = controller
            }
        }
    }
    
    private func validation(name: String, pin: String) -> Account {
        var result: Account?
        for account in accounts {
            if account.getName() == name && account.getPin() == pin {
                result = account
                break
            }
        }
        return result ?? Account(name: "", pin: "", saldo: 0)
    }
    
}
